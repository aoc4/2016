from psukys_aoc_solutions.day3 import Day3


def test1():
    data = """5 10 25"""

    expected = False
    day = Day3(data)
    assert expected == day.solution1()


def test2():
    data = """101 301 501
102 302 502
103 303 503
201 401 601
202 402 602
203 403 603"""

    expected = 6
    day = Day3(data)
    assert expected == day.solution2()
