import pytest

from psukys_aoc_solutions.day22 import Day22


def test_1_positive():
    data = """root@ebhq-gridcenter# df -h
Filesystem              Size  Used  Avail  Use%
/dev/grid/node-x0-y0     94T   73T    21T   77%
/dev/grid/node-x0-y1     40T   20T    20T   50%
/dev/grid/node-x1-y0     90T   89T    1T    99%
/dev/grid/node-x1-y1     90T   89T    1T    99%"""
    expected = 1
    sut = Day22(data=data)
    assert expected == sut.solution1()

def test_1_negative():
    data = """root@ebhq-gridcenter# df -h
Filesystem              Size  Used  Avail  Use%
/dev/grid/node-x0-y0     94T   73T    21T   77%
/dev/grid/node-x0-y1     50T   30T    20T   60%
/dev/grid/node-x1-y0     90T   89T    1T    99%
/dev/grid/node-x1-y1     90T   89T    1T    99%"""
    expected = 0
    sut = Day22(data=data)
    assert expected == sut.solution1()


def test_1_overlap():
    data = """root@ebhq-gridcenter# df -h
Filesystem              Size  Used  Avail  Use%
/dev/grid/node-x0-y0     94T   73T    21T   77%
/dev/grid/node-x0-y1     40T   20T    20T   50%
/dev/grid/node-x1-y0     50T   20T    30T   40%
/dev/grid/node-x1-y1     90T   89T    1T    99%"""
    # 0,1 to 0,0
    # 1,0 to 0,0
    # 0,1 to 1,0
    # 1,0 to 0,1
    expected = 4
    sut = Day22(data=data)
    assert expected == sut.solution1()


def test_1_zero_not_used_for_placement():
    data = """root@ebhq-gridcenter# df -h
Filesystem              Size  Used  Avail  Use%
/dev/grid/node-x0-y0     94T   73T    21T   77%
/dev/grid/node-x0-y1     40T   20T    20T   50%
/dev/grid/node-x1-y0     50T    0T    50T    0%
/dev/grid/node-x1-y1     90T   89T    1T    99%"""
    # 0,1 to 0,0
    # 0,1 to 1,0
    expected = 2
    sut = Day22(data=data)
    assert expected == sut.solution1()


@pytest.mark.skip("depends on first task solved")
def test_2():
    data = ""
    expected = None
    sut = Day22(data=data)
    assert expected == sut.solution2()
