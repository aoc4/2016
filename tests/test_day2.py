from psukys_aoc_solutions.day2 import Day2


def test1():
    data = """ULL
RRDDD
LURDL
UUUUD"""

    expected = "1985"
    day = Day2(data)
    assert expected == day.solution1()


def test2():
    data = """ULL
RRDDD
LURDL
UUUUD"""

    expected = "5DB3"
    day = Day2(data)
    assert expected == day.solution2()
