from psukys_aoc_solutions.day13 import Day13


def test_1():
    data = "10"
    expected = 11
    sut = Day13(data=data)
    assert expected == sut.solution1(target_x=7, target_y=4)


def test_2():
    data = "10"
    expected = 20
    sut = Day13(data=data)
    assert expected == sut.solution2(target_x=7, target_y=4)
