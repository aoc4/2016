import pytest

from psukys_aoc_solutions.day18 import Day18, is_safe


def test_1():
    data = ".^^.^.^^^^"
    expected = 38
    sut = Day18(data=data)
    assert expected == sut.solution1(10)


def test_1_is_safe():
    first = [True, True, False, False, True]  # ..^^.
    second = [True, False, False, False, False]  # .^^^^
    third = [False, False, True, True, False]  # ^^..^
    for index, expected in enumerate(second):
        assert expected == is_safe(first, index)

    for index, expected in enumerate(third):
        assert expected == is_safe(second, index)


@pytest.mark.skip("depends on first task solved")
def test_2():
    data = ""
    expected = None
    sut = Day18(data=data)
    assert expected == sut.solution2()
