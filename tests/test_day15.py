import pytest

from psukys_aoc_solutions.day15 import Day15


def test_1():
    data = """Disc #1 has 5 positions; at time=0, it is at position 4.
Disc #2 has 2 positions; at time=0, it is at position 1."""
    expected = 5
    sut = Day15(data=data)
    assert expected == sut.solution1()


@pytest.mark.skip("depends on first task solved")
def test_2():
    data = ""
    expected = None
    sut = Day15(data=data)
    assert expected == sut.solution2()
