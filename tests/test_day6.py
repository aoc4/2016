from psukys_aoc_solutions.day6 import Day6

DATA = """eedadn
drvtee
eandsr
raavrd
atevrs
tsrnev
sdttsa
rasrtv
nssdts
ntnada
svetve
tesnvt
vntsnd
vrdear
dvrsen
enarar"""


def test1():
    expected = "easter"
    day = Day6(DATA)
    assert expected == day.solution1()


def test2():
    expected = "advent"
    day = Day6(DATA)
    assert expected == day.solution2()
