from psukys_aoc_solutions.day8 import Day8


SIZE = (7, 3)


def test1():
    data = "rect 3x2"

    expected = 6
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test2():
    data = """rect 3x2
rotate column x=1 by 1"""

    expected = 6
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test3():
    data = """rect 3x2
rotate column x=1 by 1
rotate row y=0 by 4"""

    expected = 6
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test4():
    data = """rect 3x2
rotate column x=1 by 1
rotate row y=0 by 4
rotate column x=1 by 1"""

    expected = 6
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test5():
    data = """rect 3x2
rotate column x=1 by 1
rect 3x2
rotate row y=0 by 4
rect 3x2
rotate column x=1 by 1"""

    expected = 10
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test6():
    data = """rect 3x2
rotate column x=1 by 1
rect 3x2
rotate row y=0 by 4
rect 3x2
rotate column x=1 by 1
rect 3x2"""

    expected = 10
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test7():
    data = """rect 2x2
rotate column x=1 by 1
rect 2x2
rotate row y=0 by 12
rect 2x2
rotate column x=1 by 1
rect 2x2"""

    expected = 7
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test_rect_x_1():
    data = "rect 1x3"
    expected = 3
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test_rect_y_1():
    data = "rect 5x1"
    expected = 5
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test_row_carry_overwrite():
    data = """rect 2x2
rotate row y=1 by 2
rotate row y=1 by 2
rotate row y=1 by 2
rotate row y=1 by 1
rect 2x2"""
    expected = 4
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test_row_carry_addup():
    data = """rect 2x2
rotate row y=1 by 1
rotate row y=1 by 2
rotate row y=1 by 3
rotate row y=1 by 2
rotate row y=1 by 1
rect 2x2"""
    expected = 6
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test_column_carry_overwrite():
    data = """rect 2x2
rotate column x=1 by 1
rotate column x=1 by 1
rotate column x=1 by 1
rect 2x2"""
    expected = 4
    day = Day8(data, SIZE)
    assert expected == day.solution1()


def test_column_carry_addup():
    data = """rect 2x2
rotate column y=1 by 1
rotate column y=1 by 2
rotate column y=1 by 1
rect 2x2"""
    expected = 5
    day = Day8(data, SIZE)
    assert expected == day.solution1()
