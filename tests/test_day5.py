from psukys_aoc_solutions.day5 import Day5


def test1():
    data = "abc"
    shortcuts = [3231929, 5017308, 5278568, 5357525, 5708769, 6082117, 8036669, 8605828]
    expected = "18f47a30"
    day = Day5(data, shortcuts1=shortcuts)
    assert expected == day.solution1()


def test2():
    data = "abc"
    shortcuts = [
        3231929,
        5357525,
        5708769,
        8036669,
        8605828,
        8609554,
        13666005,
        13753421,
    ]
    expected = "05ace8e3"
    day = Day5(data, shortcuts2=shortcuts)
    assert expected == day.solution2()
