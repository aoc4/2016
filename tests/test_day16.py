import pytest

from psukys_aoc_solutions.day16 import Day16


def test_1():
    data = "10000"
    expected = "01100"
    sut = Day16(data=data)
    assert expected == sut.solution1(length=20)


@pytest.mark.skip("depends on first task solved")
def test_2():
    data = ""
    expected = None
    sut = Day16(data=data)
    assert expected == sut.solution2()
