import pytest

from psukys_aoc_solutions.day21 import Day21


def test_1():
    data = """swap position 4 with position 0
swap letter d with letter b
reverse positions 0 through 4
rotate left 1 step
move position 1 to position 4
move position 3 to position 0
rotate based on position of letter b
rotate based on position of letter d"""
    expected = "decab"
    sut = Day21(data=data)
    assert expected == sut.solution1(text="abcde")


def test_1_swap_position():
    data = "swap position 4 with position 0"
    expected = "ebcda"
    sut = Day21(data=data)
    assert expected == sut.solution1(text="abcde")


def test_1_swap_letter():
    data = "swap letter d with letter b"
    expected = "edcba"
    sut = Day21(data=data)
    assert expected == sut.solution1(text="ebcda")


def test_1_reverse_positions():
    data = "reverse positions 0 through 4"
    expected = "abcde"
    sut = Day21(data=data)
    assert expected == sut.solution1(text="edcba")


def test_rotate_left():
    data = "rotate left 1 step"
    expected = "bcdea"
    sut = Day21(data=data)
    assert expected == sut.solution1(text="abcde")


def test_rotate_right():
    # not in example
    data = "rotate right 2 steps"
    expected = "deabc"
    sut = Day21(data=data)
    assert expected == sut.solution1(text="abcde")


def test_1_move_position_1():
    data = "move position 1 to position 4"
    expected = "bdeac"
    sut = Day21(data=data)
    assert expected == sut.solution1(text="bcdea")


def test_1_move_position_2():
    data = "move position 3 to position 0"
    expected = "abdec"
    sut = Day21(data=data)
    assert expected == sut.solution1(text="bdeac")


def test_1_rotate_based_on_position_1():
    data = "rotate based on position of letter b"
    expected = "ecabd"
    sut = Day21(data=data)
    assert expected == sut.solution1(text="abdec")


def test_1_rotate_based_on_position_1():
    data = "rotate based on position of letter d"
    expected = "decab"
    sut = Day21(data=data)
    assert expected == sut.solution1(text="ecabd")


@pytest.mark.skip("depends on first task solved")
def test_2():
    data = ""
    expected = None
    sut = Day21(data=data)
    assert expected == sut.solution2()
