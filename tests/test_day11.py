import pytest

from psukys_aoc_solutions.day11 import Day11

DATA = """The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.
The second floor contains a hydrogen generator.
The third floor contains a lithium generator.
The fourth floor contains nothing relevant."""


@pytest.mark.skip("skip due to assumption on pairing requirements")
def test_1():
    expected = 11
    sut = Day11(data=DATA)
    assert expected == sut.solution1()


@pytest.mark.skip("skip due to assumption on pairing requirements")
def test_2():
    data = ""
    expected = None
    sut = Day11(data=data)
    assert expected == sut.solution2()


def test_real():
    data = """The first floor contains a polonium generator, a thulium generator, a thulium-compatible microchip, a promethium generator, a ruthenium generator, a ruthenium-compatible microchip, a cobalt generator, and a cobalt-compatible microchip.
The second floor contains a polonium-compatible microchip and a promethium-compatible microchip.
The third floor contains nothing relevant.
The fourth floor contains nothing relevant."""
    expected = 47
    sut = Day11(data=data)
    assert expected == sut.solution1()
