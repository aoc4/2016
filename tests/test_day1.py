from psukys_aoc_solutions.day1 import Day1


def test1():
    data = "R2, L3"
    expected = 5
    day = Day1(data)
    assert expected == day.solution1()


def test2():
    data = "R2, R2, R2"
    expected = 2
    day = Day1(data)
    assert expected == day.solution1()


def test3():
    data = "R5, L5, R5, R3"
    expected = 12
    day = Day1(data)
    assert expected == day.solution1()


def test4():
    data = "R8, R4, R4, R8"
    expected = 4
    day = Day1(data)
    assert expected == day.solution2()
