from psukys_aoc_solutions.day20 import Day20


def test_1():
    data = """5-8
0-2
4-7"""
    expected = 3  # also 9, but 3 is lowest
    sut = Day20(data=data)
    assert expected == sut.solution1()


def test_2():
    data = """5-8
0-2
4-7"""
    expected = 2
    sut = Day20(data=data)
    assert expected == sut.solution2(bottom=0, top=9)


def test_2_0_free():
    data = """1-2
2-3
5-6
"""
    expected = 2  # 0 and 4
    sut = Day20(data=data)
    assert expected == sut.solution2(bottom=0, top=6)
