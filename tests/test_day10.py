from psukys_aoc_solutions.day10 import Day10


DATA = """value 5 goes to bot 2
bot 2 gives low to bot 1 and high to bot 0
value 3 goes to bot 1
bot 1 gives low to output 1 and high to bot 0
bot 0 gives low to output 2 and high to output 0
value 2 goes to bot 2"""


# test steps split to whatever data is available from example
def test_1_first():
    expected = 2
    sut = Day10(data=DATA)
    assert expected == sut.solution1(compare_pair=[2, 5])


def test_1_second():
    expected = 1
    sut = Day10(data=DATA)
    assert expected == sut.solution1(compare_pair=[2, 3])


def test_1_third():
    expected = 0
    sut = Day10(data=DATA)
    assert expected == sut.solution1(compare_pair=[3, 5])


def test_2():
    expected = 30
    sut = Day10(data=DATA)
    assert expected == sut.solution2()
