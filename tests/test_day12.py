import pytest

from psukys_aoc_solutions.day12 import Day12

DATA = """cpy 41 a
inc a
inc a
dec a
jnz a 2
dec a"""


def test_1():
    expected = 42
    sut = Day12(data=DATA)
    assert expected == sut.solution1()


def test_1_negative():
    """check parser to be able to read a negative number"""
    data = """cpy -1 a
inc a
inc a
inc a
dec a
jnz a -1
dec a"""
    expected = -1
    sut = Day12(data=data)
    assert expected == sut.solution1()


@pytest.mark.skip("not applicable")
def test_2():
    data = ""
    expected = None
    sut = Day12(data=data)
    assert expected == sut.solution2()
