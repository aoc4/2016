import pytest

from psukys_aoc_solutions.day23 import Day23
import aocd

def test_1():
    data = """cpy 2 a
tgl a
tgl a
tgl a
cpy 1 a
dec a
dec a"""
    expected = 3
    sut = Day23(data=data)
    assert expected == sut.solution1()


def test_1_inc():
    data = """cpy 1 a
tgl a
inc a"""
    expected = 0
    sut = Day23(data=data)
    assert expected == sut.solution1()


def test_1_dec():
    data = """cpy 1 a
tgl a
dec a"""
    expected = 2
    sut = Day23(data=data)
    assert expected == sut.solution1()


def test_1_jnz():
    data = """cpy 1 a
tgl a
jnz 10 a"""
    expected = 10
    sut = Day23(data=data)
    assert expected == sut.solution1()

def test_1_jnz_invalid():
    data = """cpy 1 a
tgl a
jnz a 1"""  # skipped
    expected = 1
    sut = Day23(data=data)
    assert expected == sut.solution1()


def test_1_cpy():
    data = """cpy 1 a
tgl a
cpy 10 a"""
    expected = 1
    sut = Day23(data=data)
    assert expected == sut.solution1()

def test_1_tgl():
    data = """cpy 1 a
tgl a
tgl a"""  # inc
    expected = 2
    sut = Day23(data=data)
    assert expected == sut.solution1()


def test_1_tgl_self():
    data = """cpy 0 a
tgl a
jnz a -1"""
    expected = 0
    sut = Day23(data=data)
    assert expected == sut.solution1()

def test_2_real():
    data = aocd.get_data(year=2016, day=23)
    expected = 0
    sut = Day23(data=data)
    assert expected == sut.solution2()


def test_2_simple_loop():
    value = 1_000_000_000
    data = f"""cpy {value} b
    inc a
    dec b
    jnz b -2
"""
    expected = value + 12
    sut = Day23(data=data)
    assert expected == sut.solution2()

@pytest.mark.skip("depends on first task solved")
def test_2():
    data = ""
    expected = None
    sut = Day23(data=data)
    assert expected == sut.solution2()
