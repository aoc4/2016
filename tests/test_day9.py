from psukys_aoc_solutions.day9 import Day9


def test_1_no_markers():
    data = "ADVENT"
    expected = 6
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_simple_single_char_marker():
    data = "A(1x5)BC"
    expected = 7
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_simple_multi_char_marker():
    data = "(3x3)XYZ"
    expected = 9
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_multi_markers():
    data = "A(2x2)BCD(2x2)EFG"
    expected = 11
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_marker_overlap_single():
    data = "(6x1)(1x3)A"
    expected = 6
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_marker_overlap_multi():
    data = "X(8x2)(3x3)ABCY"
    expected = 18
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_2_single():
    data = "(3x3)XYZ"
    expected = 9
    sut = Day9(data=data)
    assert expected == sut.solution2()


def test_2_multiply():
    data = "X(8x2)(3x3)ABCY"
    expected = 20
    sut = Day9(data=data)
    assert expected == sut.solution2()


def test_2_boom():
    data = "(27x12)(20x12)(13x14)(7x10)(1x12)A"
    expected = 241920
    sut = Day9(data=data)
    assert expected == sut.solution2()


def test_2_boom2():
    data = "(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN"
    expected = 445
    sut = Day9(data=data)
    assert expected == sut.solution2()
