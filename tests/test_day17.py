from psukys_aoc_solutions.day17 import Day17


def test_1_1():
    data = "ihgpwlah"
    expected = "DDRRRD"
    sut = Day17(data=data)
    assert expected == sut.solution1()


def test_1_2():
    data = "kglvqrro"
    expected = "DDUDRLRRUDRD"
    sut = Day17(data=data)
    assert expected == sut.solution1()


def test_1_3():
    data = "ulqzkmiv"
    expected = "DRURDRUDDLLDLUURRDULRLDUUDDDRR"
    sut = Day17(data=data)
    assert expected == sut.solution1()


def test_2_1():
    data = "ihgpwlah"
    expected = 370
    sut = Day17(data=data)
    assert expected == sut.solution2()


def test_2_2():
    data = "kglvqrro"
    expected = 492
    sut = Day17(data=data)
    assert expected == sut.solution2()


def test_2_3():
    data = "ulqzkmiv"
    expected = 830
    sut = Day17(data=data)
    assert expected == sut.solution2()
