from psukys_aoc_solutions.day4 import Day4


def test1():
    data = """aaaaa-bbb-z-y-x-123[abxyz]
a-b-c-d-e-f-g-h-987[abcde]
not-a-real-room-404[oarel]
totally-real-room-200[decoy]"""

    expected = 1514
    day = Day4(data)
    assert expected == day.solution1()
