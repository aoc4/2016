from psukys_aoc_solutions.day19 import Day19


def test_1():
    data = "5"
    expected = 3
    sut = Day19(data=data)
    assert expected == sut.solution1()


def test_1_even():
    data = "6"
    # 1 - 1 - 2 - 4 - 0
    # 2 - 1 - 0 - 0
    # 3 - 1 - 2 - 0
    # 4 - 1 - 0 - 0
    # 5 - 1 - 2 - 6
    # 6 - 1 - 0 - 0
    expected = 5
    sut = Day19(data=data)
    assert expected == sut.solution1()


def test_2_odd():
    data = "5"
    expected = 2
    sut = Day19(data=data)
    assert expected == sut.solution2()


def test_2_even():
    data = "6"
    # 1(+) +  --------
    # 2 + (+) +  -----
    # 3 +  + (+) + (+)
    # 4 --------------
    # 5 +  -----------
    # 6 +  +  + (+) --
    expected = 3
    sut = Day19(data=data)
    assert expected == sut.solution2()
