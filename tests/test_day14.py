import pytest

from psukys_aoc_solutions.day14 import Day14


def test_1():
    data = "abc"
    expected = 22728
    sut = Day14(data=data)
    assert expected == sut.solution1()


@pytest.mark.skip("this takes some time...")
def test_2():
    data = "abc"
    expected = 22551
    sut = Day14(data=data)
    assert expected == sut.solution2()
