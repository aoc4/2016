from psukys_aoc_solutions.day7 import Day7


def test1():
    data = "abba[mnop]qrst"
    expected = 1  # abba outside
    day = Day7(data)
    assert expected == day.solution1()


def test2():
    data = "abcd[bddb]xyyx"
    expected = 0  # inside square brackets
    day = Day7(data)
    assert expected == day.solution1()


def test3():
    data = "aaaa[qwer]tyui"
    expected = 0  # no such pair
    day = Day7(data)
    assert expected == day.solution1()


def test4():
    data = "ioxxoj[asdfgh]zxcvbn"
    expected = 1  # oxxo outside
    day = Day7(data)
    assert expected == day.solution1()


def test5():
    data = "aba[bab]xyz"
    expected = 1  # aba outside, bab inside
    day = Day7(data)
    assert expected == day.solution2()


def test6():
    data = "xyx[xyx]xyx"
    expected = 0  # xyx outside, no yxy inside
    day = Day7(data)
    assert expected == day.solution2()


def test7():
    data = "aaaa[kek]eke"
    expected = 1  # eke outside, kek inside
    day = Day7(data)
    assert expected == day.solution2()


def test8():
    data = "zazbz[bzb]cdb"
    expected = 1  # zbz outside, bzb inside
    day = Day7(data)
    assert expected == day.solution2()
