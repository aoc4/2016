from copy import deepcopy
from typing import List, Tuple


class Day8:
    def __init__(self, data: str, size: Tuple[int, int] = (50, 6)):
        self.data = data.splitlines()
        self.size = size

    def solution1(self):
        row_sums = [sum(row) for row in solution1(self.data, size=self.size)]
        return sum(row_sums)

    def solution2(self):
        return solution2(self.data, size=self.size)


def enable_rect(instruction: str, screen: List[List[bool]]):
    width, height = [int(x) for x in instruction.split()[1].split("x")]
    new_screen = deepcopy(screen)
    for row in range(len(screen)):
        for column in range(len(screen[row])):
            if row < height and column < width:
                new_screen[row][column] = True
    return new_screen


def rotate_row(instruction: str, screen: List[List[bool]]):
    row, steps = [int(x) for x in instruction.split("=")[1].split(" by ")]
    new_screen = deepcopy(screen)
    row_length = len(screen[row])
    for y in range(row_length):
        new_screen[row][(y + steps) % row_length] = screen[row][y]
    return new_screen


def rotate_column(instruction: str, screen: List[List[bool]]):
    column, steps = [int(x) for x in instruction.split("=")[1].split(" by ")]
    new_screen = deepcopy(screen)
    col_length = len(screen)
    for x in range(col_length):
        new_screen[(x + steps) % col_length][column] = screen[x][column]
    return new_screen


def solution1(instructions: List[str], size: Tuple[int, int] = (50, 6)) -> int:
    screen = [[False for _ in range(size[0])] for _ in range(size[1])]
    for instruction in instructions:
        if "rect" in instruction:
            screen = enable_rect(instruction, screen)
        elif "rotate row" in instruction:
            screen = rotate_row(instruction, screen)
        elif "rotate column" in instruction:
            screen = rotate_column(instruction, screen)
    return screen


def solution2(instructions: List[str], size: Tuple[int, int] = (50, 6)) -> int:
    transformed = [
        ["." if item else " " for item in row] for row in solution1(instructions, size)
    ]
    return "\n" + "\n".join(["".join(row) for row in transformed])
