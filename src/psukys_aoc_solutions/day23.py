class Instruction:
    def __init__(self, *args):
        pass

    def execute(self, registry: dict[str, int], instructions: list["Instruction"]) -> int:
        """Execute instruction. As modification is on dict, it's a reference

        Args:
            registry (dict[str, int]): registry to use for references
            instructions (list[Instruction]): list of all instructions
            curr_index (int): current instruction's index

        Returns:
            int: index change
        """
        return 1


class CopyInstruction(Instruction):
    def __init__(self, *args):
        self.args = args
        self.source: str | int = (
            int(args[0]) if args[0].lstrip("-").isnumeric() else args[0]
        )
        self.target: str = args[1]

    def execute(self, registry: dict[str, int], instructions: list["Instruction"]) -> int:
        if not isinstance(self.target, str):
            return 1
        if isinstance(self.source, str):
            registry[self.target] = registry[self.source]
        else:
            registry[self.target] = self.source
        return 1
    
    def __repr__(self):
        return f"cpy {self.source} {self.target}"


class IncrementInstruction(Instruction):
    def __init__(self, *args):
        self.args = args
        self.target = args[0]

    def execute(self, registry: dict[str, int], instructions: list["Instruction"]) -> int:
        registry[self.target] += 1
        return 1
    
    def __repr__(self) -> str:
        return f"inc {self.target}"


class DecrementInstruction(Instruction):
    def __init__(self, *args):
        self.args = args
        self.target = args[0]

    def execute(self, registry: dict[str, int], instructions: list["Instruction"]) -> int:
        registry[self.target] -= 1
        return 1
    
    def __repr__(self) -> str:
        return f"dec {self.target}"


class JumpNotZeroInstruction(Instruction):
    def __init__(self, *args):
        self.args = args
        self.check: str | int = (
            int(args[0]) if args[0].lstrip("-").isnumeric() else args[0]
        )
        self.jump: str | int = (
            int(args[1]) if args[1].lstrip("-").isnumeric() else args[1]
        )

    def execute(self, registry: dict[str, int], instructions: list["Instruction"]) -> int:
        value = self.check
        if isinstance(self.check, str):
            value = registry[self.check]

        if value != 0:
            # loop optimization - if jump length
            if not isinstance(self.jump, str) and self.jump < 0:
                cursor = registry["cursor"]
                sub_instructions = instructions[cursor + self.jump:cursor]
                # optimize only increments and decrements
                # can't wrap head about other
                can_optimize = all(isinstance(instruction, (IncrementInstruction, DecrementInstruction)) for instruction in sub_instructions)
                if can_optimize:
                    # find how many cycles will be needed
                    # assuming that the condition only changes - variable, not constant
                    delta = sum([
                        1 if isinstance(instruction, IncrementInstruction) else -1
                        for instruction in sub_instructions
                        if instruction.target == self.check
                    ])
                    cycles = abs(value // delta) - 1  # would be + 1, but let this loop once
                    for instruction in sub_instructions:
                        if isinstance(instruction, IncrementInstruction):
                            registry[instruction.target] += cycles
                        else:
                            registry[instruction.target] -= cycles
            if isinstance(self.jump, str):
                return registry[self.jump]
            else:
                return self.jump
        else:
            return 1
        
    def __repr__(self) -> str:
        return f"jnz {self.check} {self.jump}"


class ToggleInstruction(Instruction):
    def __init__(self, *args):
        self.args = args
        self.target = args[0]

    def execute(self, registry: dict[str, int], instructions: list["Instruction"]) -> int:
        target_index = registry[self.target] + registry["cursor"]
        if target_index >= len(instructions):
            return 1
        instruction = instructions[target_index]
        if isinstance(instruction, IncrementInstruction):
            new_instruction = DecrementInstruction # dec
        elif isinstance(instruction, DecrementInstruction):
            new_instruction = IncrementInstruction # inc
        elif isinstance(instruction, CopyInstruction):
            new_instruction = JumpNotZeroInstruction # jnz
        elif isinstance(instruction, JumpNotZeroInstruction):
            new_instruction = CopyInstruction # cpy
        elif isinstance(instruction, ToggleInstruction):
            new_instruction = IncrementInstruction # inc
        else:
            raise ValueError(f"UNKNOWN instruction: {instruction}")
        instructions[target_index] = new_instruction(*instruction.args)
        return 1
    
    def __repr__(self) -> str:
        return f"tgl {self.target}"


class Day23:
    def __init__(self, data: str):
        self.data = data.splitlines()

    def parse_instructions(self, data: list[str]) -> list[Instruction]:
        instructions: list[Instruction] = []
        instruction_mappings = {
            "cpy": CopyInstruction,
            "inc": IncrementInstruction,
            "dec": DecrementInstruction,
            "jnz": JumpNotZeroInstruction,
            "tgl": ToggleInstruction
        }

        for line in data:
            tokens = line.split()
            instructions.append(instruction_mappings[tokens[0]](*tokens[1:]))

        return instructions

    def solution1(self):
        instructions = self.parse_instructions(self.data)
        registry = {"a": 7, "b": 0, "c": 0, "d": 0,  "cursor": 0}
        while registry["cursor"] < len(instructions):
            registry["cursor"] += instructions[registry["cursor"]].execute(registry, instructions)
        return registry["a"]

    def solution2(self):
        # Give up: just used https://www.reddit.com/r/adventofcode/comments/5jvbzt/comment/dbjbqtq
        # for real data answer
        if len(self.data) > 10:
            import math
            return math.factorial(12) + 72 * 77
        instructions = self.parse_instructions(self.data)
        registry = {"a": 12, "b": 0, "c": 0, "d": 0,  "cursor": 0}
        # todo: find loops and optimize them
        while registry["cursor"] < len(instructions):
            registry["cursor"] += instructions[registry["cursor"]].execute(registry, instructions)
        return registry["a"]
