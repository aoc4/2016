import re
import itertools


def swap_position(text: str, x: int, y: int) -> str:
    text_as_list = list(text)
    text_as_list[x], text_as_list[y] = text_as_list[y], text_as_list[x]
    return ''.join(text_as_list)


def swap_letter(text: str, x: str, y: str) -> str:
    x_idx = text.index(x)
    y_idx = text.index(y)
    return swap_position(text, x_idx, y_idx)


def rotate_left(text: str, step: int) -> str:
    return text[step % len(text):] + text[:step % len(text)]


def rotate_right(text: str, step: int) -> str:
    return text[-step % len(text):] + text[:-step % len(text)]


def rotate_based_on_position(text: str, letter: str) -> str:
    idx = text.index(letter)
    if idx >= 4:
        idx += 1
    return rotate_right(text, idx + 1)


def reverse_positions(text: str, x: int, y: int) -> str:
    y += 1  # to match a and not include in concat
    return text[:x] + text[x:y][::-1] + text[y:]


def move_position(text: str, x: int, y: int) -> str:
    char = text[x]
    text = text[:x] + text[x + 1:]
    text = text[:y] + char + text[y:]
    return text


class Day21:
    def __init__(self, data: str):
        self.data = data.splitlines()

    def solution1(self, text: str="abcdefgh"):
        for line in self.data:
            if match := re.match(r"swap position (\d+) with position (\d+)", line):
                text = swap_position(text, int(match.group(1)), int(match.group(2)))
            elif match := re.match(r"swap letter (\S+) with letter (\S+)", line):
                text = swap_letter(text, match.group(1), match.group(2))
            elif match := re.match(r"rotate left (\d+) step[s]?", line):
                text = rotate_left(text, int(match.group(1)))
            elif match := re.match(r"rotate right (\d+) step[s]?", line):
                text = rotate_right(text, int(match.group(1)))
            elif match := re.match(r"rotate based on position of letter (\S+)", line):
                text = rotate_based_on_position(text, match.group(1))
            elif match := re.match(r"reverse positions (\d+) through (\d+)", line):
                text = reverse_positions(text, int(match.group(1)), int(match.group(2)))
            elif match := re.match(r"move position (\d+) to position (\d+)", line):
                text = move_position(text, int(match.group(1)), int(match.group(2)))
            else:
                raise ValueError(f"Unknown operation: {line}")
        return text


    def solution2(self, password: str="fbgdceah"):
        # now to reverse everything...?
        # instead let's just generate all combinations and see what the original value was
        for combination in itertools.permutations("abcdefgh"):
            if self.solution1(text=combination) == password:
                return ''.join(combination)
        raise ValueError("No combination matches")