class Day20:
    def __init__(self, data: str):
        self.ranges = [
            [int(item) for item in line.split("-")] for line in data.splitlines()
        ]

        last_size = -1
        while last_size != len(self.ranges):
            last_size = len(self.ranges)
            self.reduce_ranges()
            self.ranges = sorted(self.ranges, key=lambda x: x[0])
            self.merge_ranges()

    def reduce_ranges(self):
        old_ranges = self.ranges.copy()
        new_ranges = []
        for old in old_ranges:
            for ref_idx, ref in enumerate(new_ranges):
                if ref[0] <= old[0] <= ref[1] or ref[0] <= old[1] <= ref[1]:
                    new_ranges[ref_idx] = [min(ref[0], old[0]), max(ref[1], old[1])]
                    break
            else:
                new_ranges.append(old)
        self.ranges = new_ranges

    def merge_ranges(self):
        old_ranges = self.ranges.copy()
        new_ranges = []
        for first, second in zip(old_ranges[::2], old_ranges[1::2]):
            if first[1] + 1 == second[0]:
                new_ranges.append([first[0], second[1]])
            else:
                new_ranges.append(first)
                new_ranges.append(second)
        if len(old_ranges) % 2 != 0:
            new_ranges.append(old_ranges[-1])
        self.ranges = new_ranges

    def solution1(self):
        if self.ranges[0][0] != 0:
            return 0

        # merge to first range until can't
        end = self.ranges[0][1]
        index = 1
        while end + 1 == self.ranges[index][0]:
            end = self.ranges[index][1]
            index += 1
        return end + 1

    def solution2(self, bottom: int = 0, top: int = 4294967295):
        free_ip_ranges = []
        if self.ranges[0][0] != bottom:
            free_ip_ranges.append([bottom, self.ranges[0][0] - 1])

        bottom = self.ranges[0][1] + 1

        for r in self.ranges[1:-1]:
            free_ip_ranges.append([bottom, r[0] - 1])
            bottom = r[1] + 1

        if self.ranges[1:-1] == []:
            free_ip_ranges.append([bottom, bottom])

        if self.ranges[-1][1] != top:
            free_ip_ranges.append([self.ranges[-1][1] + 1, top])

        # calculate ranges

        return sum([free_range[1] - free_range[0] + 1 for free_range in free_ip_ranges])
