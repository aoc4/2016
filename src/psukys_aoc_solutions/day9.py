from dataclasses import dataclass
import re
from typing import Iterable


class Day9:
    def __init__(self, data: str):
        self.data = data

    def solution1(self):
        return len(solution1(data=self.data))

    def solution2(self):
        return solution2(self.data)


@dataclass
class Marker:
    span: int
    multiplier: int


def _get_markers(data: str) -> Iterable[tuple[int, int, Marker]]:
    for match in re.finditer(r"\((\d+)x(\d+)\)", data):
        yield (
            match.start(),
            match.end(),
            Marker(int(match.group(1)), int(match.group(2))),
        )


def solution1(data: str) -> str:
    solution = ""
    index = 0
    for start, end, marker in _get_markers(data):
        if start < index:
            continue
        if start > index:
            solution += data[index:start]
        solution += data[end : end + marker.span] * marker.multiplier
        index = end + marker.span

    if index < len(data):
        solution += data[index:]

    return solution


def solution2(data: str) -> int:
    count = 0
    index = 0
    for start, end, marker in _get_markers(data):
        if start < index:
            continue
        if index < start:
            count += start - index
        count += marker.multiplier * solution2(data[end : end + marker.span])
        index = end + marker.span

    count += len(data) - index

    return count
