def is_safe(line: list[bool], index: int) -> bool:
    left_safe = True if index == 0 else line[index - 1]
    mid_safe = line[index]
    right_safe = True if index + 1 == len(line) else line[index + 1]
    is_trap = (
        (not left_safe and not mid_safe and right_safe)
        ^ (left_safe and not mid_safe and not right_safe)
        ^ (not left_safe and mid_safe and right_safe)
        ^ (left_safe and mid_safe and not right_safe)
    )
    return not is_trap


def to_original(rows: list[list[bool]]) -> list[str]:
    return ["".join("." if safe else "^" for safe in row) for row in rows]


class Day18:
    def __init__(self, data: str):
        self.data = [char == "." for char in data]

    def solution1(self, amount: int = 40):
        rows = [self.data]
        for _ in range(amount - 1):
            row_above = rows[-1]
            rows.append([is_safe(row_above, index) for index in range(len(row_above))])
        return sum([sum(row) for row in rows])

    def solution2(self):
        return self.solution1(400_000)
