import re


class Day22:
    def __init__(self, data: str):
        self.grid = {}
        lines = data.splitlines()[2:]
        for line in lines:
            match = re.match(r"/dev/grid/node-x(?P<x>\d+)-y(?P<y>\d+)\s+(?P<size>\d+)T\s+(?P<used>\d+)T\s+(?P<avail>\d+)T.*", line)
            self.grid[(match.group("x"), match.group("y"))] = {"used": int(match.group("used")), "available": int(match.group("avail"))}

    def solution1(self):
        count = 0
        # can I fit item in other
        for _, item in self.grid.items():
            for _, other in self.grid.items():
                if item == other:
                    continue
                if item["used"] != 0 and item["used"] <= other["available"]:
                    count += 1
        return count

    def solution2(self):
        # solved by hand
        pass