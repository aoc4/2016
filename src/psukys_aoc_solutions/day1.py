from typing import List, Tuple


class Day1:
    def __init__(self, data: str):
        self.data = data

    def solution1(self):
        return solution1(self.data)

    def solution2(self):
        return solution2(self.data)


def update_direction(old_direction: str, turn: str) -> str:
    directions = "NESW"
    idx = directions.index(old_direction)

    if turn == "L":
        idx = idx - 1
    else:
        idx = (idx + 1) % 4

    return directions[idx]


def update_coordinates(x: int, y: int, direction: str, steps: int) -> Tuple[int, int]:
    if direction == "N":
        y += steps
    elif direction == "S":
        y -= steps
    elif direction == "W":
        x -= steps
    elif direction == "E":
        x += steps
    return x, y


def solution1(data: str) -> int:
    steps = data.split(", ")
    x = y = 0
    current_direction = "N"
    for step in steps:
        turn = step[0]
        steps = int(step[1:])
        current_direction = update_direction(current_direction, turn)
        x, y = update_coordinates(x, y, current_direction, steps)

    return abs(x) + abs(y)


def get_coordinates(start: int, end: int) -> List[int]:
    if start > end:
        return list(range(end, start))
    else:
        return list(range(start + 1, end + 1))


def get_visited_locations(
    start_x: int, start_y: int, end_x: int, end_y: int
) -> List[Tuple[int, int]]:
    if start_x == end_x:
        return [(end_x, y) for y in get_coordinates(start_y, end_y)]
    else:
        return [(x, end_y) for x in get_coordinates(start_x, end_x)]


def solution2(data: str) -> int:
    steps = data.split(", ")
    locations = set([(0, 0)])
    x = y = 0
    current_direction = "N"
    for step in steps:
        turn = step[0]
        steps = int(step[1:])
        current_direction = update_direction(current_direction, turn)
        new_x, new_y = update_coordinates(x, y, current_direction, steps)
        new_locations = get_visited_locations(x, y, new_x, new_y)
        # new_locations.remove((new_x, new_y))
        for location in new_locations:
            if location in locations:
                return abs(location[0]) + abs(location[1])
        locations.update(set(new_locations))
        x, y = new_x, new_y

    raise Exception("No double visits")
