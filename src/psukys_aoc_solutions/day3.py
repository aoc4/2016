class Day3:
    def __init__(self, data: str):
        self.data = data

    def solution1(self):
        return sum(solution1(line) for line in self.data.splitlines())

    def solution2(self):
        return solution2(self.data)


def is_triangle(a: int, b: int, c: int):
    return a < (b + c) and b < (a + c) and c < (a + b)


def solution1(line: str) -> bool:
    a, b, c = [int(x) for x in line.split()]
    return is_triangle(a, b, c)


def solution2(data: str) -> int:
    lines = [[int(x) for x in line.split()] for line in data.splitlines()]
    triangles = 0
    for idx in range(0, len(lines), 3):
        for x in range(3):
            triangles += is_triangle(
                lines[idx][x], lines[idx + 1][x], lines[idx + 2][x]
            )
    return triangles
