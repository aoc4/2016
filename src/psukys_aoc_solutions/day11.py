class Day11:
    def __init__(self, data: str):
        self.data: list[str] = data.splitlines()

    def parse_floors(self, data: list[str]) -> list[int]:
        floors = []
        for line in data:
            component_token = line.split("contains ")[1]
            if component_token == "nothing relevant.":
                floors.append(0)
            else:
                # separated by commas and an inconsistent oxford comma (thanks) for last item
                if "," in component_token:
                    floors.append(len(component_token.split(",")))
                else:  # separated by "and"
                    floors.append(2)
        return floors

    def solution1(self) -> int:
        """How many floors has the elevator went up and down.

        Assumption is on safety to move all items from one floor to other and not requiring additional actions.
        Formula: sum(2 * (sum([floor0_comp_count, f1, f2, f3][:curr_floor]) - 3 for curr_floor in 1..4)

        -3 because of last move

        Returns:
            int: floors moved by elevator
        """
        floors = self.parse_floors(self.data)
        return sum(2 * sum(floors[:curr_floor]) - 3 for curr_floor in range(1, 4))

    def solution2(self):
        """Same as solution1 just simulate adding the two pairs"""
        floors = self.parse_floors(self.data)
        floors[0] += 4
        return sum(2 * sum(floors[:curr_floor]) - 3 for curr_floor in range(1, 4))
