from typing import Counter, Tuple

import re

from string import ascii_lowercase


class Day4:
    def __init__(self, data: str):
        self.data = data

    def solution1(self):
        return solution1(self.data)

    def solution2(self):
        return solution2(self.data)


def cleanup_room(name: str) -> str:
    return "".join(c for c in name if c in ascii_lowercase)


def parse_line(line: str) -> Tuple[str, int, str]:
    match = re.match(r"(\S+)-(\d{3})\[(\S+)\]", line)
    return match.group(1), int(match.group(2)), match.group(3)


def is_valid(room: Tuple[str, int, str]) -> bool:
    counter = Counter(sorted(cleanup_room(room[0])))
    return set(pair[0] for pair in counter.most_common(5)) == set(room[2])


def solution1(data: str) -> int:
    rooms = [parse_line(line) for line in data.splitlines()]
    valid_rooms = [room for room in rooms if is_valid(room)]
    return sum(room[1] for room in valid_rooms)


def decrypt(name: str, sector: int) -> str:
    new_name = ""
    for c in name:
        if c in ascii_lowercase:
            idx = ascii_lowercase.index(c)
            new_idx = (idx + sector) % len(ascii_lowercase)
            new_name += ascii_lowercase[new_idx]
        elif c == "-":
            new_name += " "
    return new_name


def solution2(data: str) -> int:
    rooms = [parse_line(line) for line in data.splitlines()]
    valid_rooms = [room for room in rooms if is_valid(room)]
    decrypted_names = [(decrypt(room[0], room[1]), room[1]) for room in valid_rooms]
    # no clear description what to search for
    # really just a guess after looking at data
    return [sector for name, sector in decrypted_names if "northpole" in name][0]
