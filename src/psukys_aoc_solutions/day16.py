class Day16:
    def __init__(self, data: str):
        self.data = data

    def dragon_curve(self, data: str) -> str:
        final = data + "0"
        for char in data[::-1]:
            final += "1" if char == "0" else "0"
        return final

    def hash_sum(self, data: str) -> str:
        final = ""
        for left, right in zip(data[::2], data[1::2]):
            final += "1" if left == right else "0"
        return final

    def solution1(self, length: int = 272):
        generated = self.data
        while len(generated) < length:
            generated = self.dragon_curve(generated)

        generated = generated[:length]

        checksum = self.hash_sum(generated)
        while len(checksum) % 2 == 0:
            checksum = self.hash_sum(checksum)
        return checksum

    def solution2(self):
        return self.solution1(35651584)
