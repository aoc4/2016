class Day13:
    def __init__(self, data: str):
        self.fav_number = int(data)

    def get_formula_sum(self, x: int, y: int):
        return x * x + 3 * x + 2 * x * y + y + y * y + self.fav_number

    def is_formula_bit_sum_even(self, x: int, y: int) -> bool:
        formula_sum = self.get_formula_sum(x, y)
        return formula_sum.bit_count() % 2 == 0

    def get_neighbors(self, x: int, y: int) -> list[tuple[int, int]]:
        # failsafe: don't go into negative bounds.
        # Using max(0, possibly_negative) ends up with duplicates
        # Use set to keep only unique values
        neighbors = {
            (max(0, x - 1), y),
            (x, max(0, y - 1)),
            (x, y + 1),
            (x + 1, y),
        }
        if (x, y) in neighbors:
            neighbors.remove((x, y))

        return [coords for coords in neighbors if self.is_formula_bit_sum_even(*coords)]

    def get_traversed_office(
        self, target_x: int, target_y: int
    ) -> dict[tuple[int, int], int]:
        """Traverses office until target coordinates are met and
        provides map of how many steps are from (1, 1) location.

        Arguments:
            target_x (int): x coordinate
            target_y (int): y coordinate

        Returns:
            dict[tuple[int, int], int]: dictionary of coordinates as keys, amount of step to reach as value

        """
        START = (1, 1)
        END = (target_x, target_y)

        office: dict[tuple[int, int], int] = {START: 0}
        buffer: list[tuple[int, int]] = self.get_neighbors(*START)
        step = 1

        while END not in office:
            # BFS
            new_buffer = []
            for neighbor in buffer:
                if neighbor not in office:  # don't override smaller value
                    office[neighbor] = step
                new_buffer.extend(
                    [n for n in self.get_neighbors(*neighbor) if n not in office]
                )

            step += 1
            buffer = new_buffer

        return office

    def solution1(self, target_x: int = 31, target_y: int = 39):
        office = self.get_traversed_office(target_x, target_y)
        return office[(target_x, target_y)]

    def solution2(self, target_x: int = 31, target_y: int = 39):
        office = self.get_traversed_office(target_x, target_y)
        return sum(1 for _, steps in office.items() if steps <= 50)
