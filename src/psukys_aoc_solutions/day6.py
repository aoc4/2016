from typing import Dict, List


class Day6:
    def __init__(self, data: str):
        self.data = data.splitlines()

    def solution1(self):
        return solution1(self.data)

    def solution2(self):
        return solution2(self.data)


def _count_letters(messages: List[str]) -> List[Dict[str, int]]:
    # create initial structure:
    # list represents word by letter (at index 0 - first letter)
    # each element in list is a dictionary with <letter:count> pairs
    counter: List[Dict[str, int]] = [{} for _ in range(len(messages[0]))]
    for message in messages:
        for index, letter in enumerate(message):
            count = counter[index].get(letter, 0)
            counter[index][letter] = count + 1
    return counter


def solution1(messages: List[str]) -> str:
    counter = _count_letters(messages)
    return "".join(max(letters, key=letters.get) for letters in counter)


def solution2(messages: List[str]) -> str:
    counter = _count_letters(messages)
    return "".join(min(letters, key=letters.get) for letters in counter)
