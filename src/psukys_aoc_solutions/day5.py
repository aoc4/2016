import hashlib
from typing import List

SOLUTION1_SHORTCUTS = [
    4515059,
    6924074,
    8038154,
    13432968,
    13540621,
    14095580,
    14821988,
    16734551,
]

SOLUTION2_SHORTCUTS = [
    4515059,
    8038154,
    13432968,
    17743256,
    19112977,
    20616595,
    21658552,
    26326685,
]


class Day5:
    def __init__(
        self, data: str, shortcuts1=SOLUTION1_SHORTCUTS, shortcuts2=SOLUTION2_SHORTCUTS
    ):
        self.data = data
        self.shortcuts1 = shortcuts1
        self.shortcuts2 = shortcuts2

    def solution1(self):
        return solution1(self.data, shortcuts=self.shortcuts1)

    def solution2(self):
        return solution2(self.data, shortcuts=self.shortcuts2)


def get_hash(base: str, n: int) -> str:
    m = hashlib.md5()
    m.update(str(base + str(n)).encode("utf-8"))
    return m.hexdigest()


def solution1(data: str, shortcuts: List[int] = []) -> str:
    password = ""
    num = 0
    while len(password) != 8:
        if shortcuts:
            num = shortcuts[len(password)]
        hash = get_hash(data, num)
        if hash.startswith("0" * 5):
            password += hash[5]
        num += 1

    return password


def solution2(data: str, shortcuts: List[int] = []) -> str:
    password = ["_"] * 8
    num = 0
    while sum([item != "_" for item in password]) != 8:
        if shortcuts:
            num = shortcuts[sum([item != "_" for item in password])]
        hash = get_hash(data, num)
        idx = int(hash[5], 16)
        if hash.startswith("0" * 5) and idx < 8 and password[idx] == "_":
            password[idx] = hash[6]
        num += 1

    return "".join(password)
