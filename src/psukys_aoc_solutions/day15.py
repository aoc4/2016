import re


class Day15:
    def __init__(self, data: str):
        self.discs: list[tuple[int, int]] = self.parse_data(data)

    def parse_data(self, data: str) -> list[tuple[int, int]]:
        discs = []
        data_matcher = re.compile(
            r"has (\d+) position[s]?; at time=0, it is at position (\d+)"
        )
        for positions, start in data_matcher.findall(data):
            discs.append((int(positions), int(start)))
        return discs

    def solution1(self):
        time = 0
        while True:
            # just find a pass time and see if others can pass + their offset
            cycle_pass_time = None
            for offset, (positions, start) in enumerate(self.discs, 1):
                pass_time = (start + offset + time) % positions
                if cycle_pass_time is None:
                    cycle_pass_time = pass_time
                elif cycle_pass_time != pass_time:  #
                    break
            else:
                return time
            time += 1

    def solution2(self):
        self.discs.append((11, 0))
        return self.solution1()
