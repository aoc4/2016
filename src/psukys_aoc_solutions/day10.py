from dataclasses import dataclass, field
import re
from collections import defaultdict


@dataclass
class Bot:
    low_id: int = -1
    low_type: str = ""
    high_id: int = -1
    high_type: str = ""
    chips: list[int] = field(default_factory=list)


class Day10:
    def __init__(self, data: str):
        self.data = data

    def parse_data(self, data: list[str]) -> dict[int, Bot]:
        bots = defaultdict(Bot)
        for line in data:
            if line.startswith("value"):  # chip assignment
                parsed = re.match("value (\d+) goes to bot (\d+)", line)
                chip_value, bot_id = parsed.groups()
                bots[int(bot_id)].chips.append(int(chip_value))
            elif line.startswith("bot"):  # bot logic
                parsed = re.match(
                    "bot (\d+) gives low to (\S+) (\d+) and high to (\S+) (\d+)", line
                )
                bot_id, low_type, low_id, high_type, high_id = parsed.groups()
                bot_id = int(bot_id)
                bots[bot_id].high_id = int(high_id)
                bots[bot_id].high_type = high_type
                bots[bot_id].low_id = int(low_id)
                bots[bot_id].low_type = low_type
            else:
                raise RuntimeError("Unknown parsing case")
        return bots

    def solution1(self, compare_pair: list[int] = [61, 17]):
        bots: dict[int, Bot] = self.parse_data(self.data.splitlines())
        outputs = defaultdict(list)
        while True:
            for bot_id, bot in bots.items():
                if len(bot.chips) >= 2:
                    bot.chips = sorted(bot.chips)
                    low_value = bot.chips.pop(0)
                    high_value = bot.chips.pop(-1)
                    if bot.high_type == "bot":
                        bots[bot.high_id].chips.append(high_value)
                    else:
                        outputs[bot.high_id].append(high_value)

                    if bot.low_type == "bot":
                        bots[bot.low_id].chips.append(low_value)
                    else:
                        outputs[bot.low_id].append(low_value)

                    if sorted([low_value, high_value]) == sorted(compare_pair):
                        return bot_id

    def solution2(self):
        bots: dict[int, Bot] = self.parse_data(self.data.splitlines())
        outputs = defaultdict(lambda: -1)
        while True:
            for _, bot in bots.items():
                if len(bot.chips) >= 2:
                    bot.chips = sorted(bot.chips)
                    low_value = bot.chips.pop(0)
                    high_value = bot.chips.pop(-1)
                    if bot.high_type == "bot":
                        bots[bot.high_id].chips.append(high_value)
                    else:
                        outputs[bot.high_id] = high_value

                    if bot.low_type == "bot":
                        bots[bot.low_id].chips.append(low_value)
                    else:
                        outputs[bot.low_id] = low_value
            if outputs[0] != -1 and outputs[1] != -1 and outputs[2] != -1:
                return outputs[0] * outputs[1] * outputs[2]
