from hashlib import md5

UP = "U"
DOWN = "D"
LEFT = "L"
RIGHT = "R"


def can_go(coordinates: tuple[int, int], direction: str) -> bool:
    if direction == UP and coordinates[1] == 1:
        return False
    if direction == DOWN and coordinates[1] == 4:
        return False
    if direction == LEFT and coordinates[0] == 1:
        return False
    if direction == RIGHT and coordinates[0] == 4:
        return False
    return True


def path_to_coordinates(path: str) -> tuple[int, int]:
    x = 1
    y = 1
    for char in path:
        if char == UP:
            y -= 1
        elif char == DOWN:
            y += 1
        elif char == LEFT:
            x -= 1
        elif char == RIGHT:
            x += 1
    return x, y


class Day17:
    def __init__(self, data: str):
        self.data = data

    def solution1(self):
        # bfs
        open_chars = "bcdef"
        vault = (4, 4)
        buffer = [self.data]
        while buffer != []:
            new_buffer = []
            for path in buffer:
                coordinates = path_to_coordinates(path)
                if coordinates == vault:
                    return path[len(self.data) :]
                hash = md5(path.encode("utf-8")).hexdigest()[:4]
                for index, direction in enumerate([UP, DOWN, LEFT, RIGHT]):
                    is_open = hash[index] in open_chars
                    if is_open and can_go(coordinates, direction):
                        new_buffer.append(path + direction)
            buffer = new_buffer
        raise ValueError("No path found")

    def solution2(self):
        # bfs
        valid_path_lengths = []
        open_chars = "bcdef"
        vault = (4, 4)
        buffer = [self.data]
        while buffer != []:
            new_buffer = []
            for path in buffer:
                coordinates = path_to_coordinates(path)
                if coordinates == vault:
                    valid_path_lengths.append(len(path[len(self.data) :]))
                    continue  # don't move from this room anymore
                hash = md5(path.encode("utf-8")).hexdigest()[:4]
                for index, direction in enumerate([UP, DOWN, LEFT, RIGHT]):
                    is_open = hash[index] in open_chars
                    if is_open and can_go(coordinates, direction):
                        new_buffer.append(path + direction)
            buffer = new_buffer
        return max(valid_path_lengths)
