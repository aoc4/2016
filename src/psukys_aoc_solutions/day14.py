from functools import cache
import hashlib
import re


@cache
def hash(text: str) -> str:
    return hashlib.md5(text.encode("utf-8")).hexdigest()


class Day14:
    def __init__(self, data: str):
        self.salt = data

    def stretch_hash(self, text: str):
        my_hash = hash(text)
        for _ in range(2016):
            my_hash = hash(my_hash)
        return my_hash

    def solution1(self):
        three = re.compile(r"(\w)\1{2}")
        five = re.compile(r"(\w)\1{4}")

        index = 0
        hashes = set()

        while len(hashes) < 64:
            five_matches = five.findall(
                hashlib.md5(
                    f"{self.salt}{index}".encode("utf-8"), usedforsecurity=False
                ).hexdigest()
            )
            for five_match in set(five_matches):
                for supindex in range(max(index - 1000, 0), index):
                    three_matches = three.search(
                        hashlib.md5(
                            f"{self.salt}{supindex}".encode("utf-8"),
                            usedforsecurity=False,
                        ).hexdigest()
                    )
                    if three_matches and five_match == three_matches.group(1):
                        hashes.add(supindex)
            index += 1

        return sorted(hashes)[63]

    def solution2(self):
        three = re.compile(r"(\w)\1{2}")
        five = re.compile(r"(\w)\1{4}")

        index = 0
        hashes = set()

        while len(hashes) < 64:
            five_matches = five.findall(self.stretch_hash(f"{self.salt}{index}"))
            for five_match in set(five_matches):
                for supindex in range(max(index - 1000, 0), index):
                    three_matches = three.search(
                        self.stretch_hash(f"{self.salt}{supindex}")
                    )
                    if three_matches and five_match == three_matches.group(1):
                        hashes.add(supindex)
            index += 1

        return sorted(hashes)[63]
