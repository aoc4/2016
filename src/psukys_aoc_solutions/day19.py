import math


class Day19:
    def __init__(self, data: str):
        self.amount = int(data)

    def solution1(self):
        elves = list(range(1, self.amount + 1))
        gifts = [1 for _ in range(self.amount)]
        while len(elves) != 1:
            new_elves = []
            new_gifts = []
            for left, right in zip(range(0, len(elves), 2), range(1, len(elves), 2)):
                new_elves.append(elves[left])
                new_gifts.append(gifts[left] + gifts[right])
            if len(elves) % 2 != 0:  # last was not visited
                # last takes first's gifts
                new_elves.append(elves[-1])
                new_elves.pop(0)
                new_gifts.append(gifts[-1] + new_gifts.pop(0))
            elves = new_elves
            gifts = new_gifts
        return elves[0]

    def solution2(self):
        # Josephus problem
        # naive throttles on array operations
        p = 3 ** int(math.log(self.amount - 1, 3))
        return self.amount - p + max(self.amount - 2 * p, 0)
