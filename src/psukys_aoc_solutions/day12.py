class Instruction:
    def __init__(self, *args):
        pass

    def execute(self, registry: dict[str, int]) -> int:
        """Execute instruction. As modification is on dict, it's a reference

        Args:
            registry (dict[str, int]): registry to use for references

        Returns:
            int: index change
        """
        return 1


class CopyInstruction(Instruction):
    def __init__(self, *args):
        self.source: str | int = (
            int(args[0]) if args[0].lstrip("-").isnumeric() else args[0]
        )
        self.target: str = args[1]

    def execute(self, registry: dict[str, int]) -> int:
        if isinstance(self.source, str):
            registry[self.target] = registry[self.source]
        else:
            registry[self.target] = self.source
        return 1


class IncrementInstruction(Instruction):
    def __init__(self, *args):
        self.target = args[0]

    def execute(self, registry: dict[str, int]) -> int:
        registry[self.target] += 1
        return 1


class DecrementInstruction(Instruction):
    def __init__(self, *args):
        self.target = args[0]

    def execute(self, registry: dict[str, int]) -> int:
        registry[self.target] -= 1
        return 1


class JumpNotZeroInstruction(Instruction):
    def __init__(self, *args):
        self.check: str | int = (
            int(args[0]) if args[0].lstrip("-").isnumeric() else args[0]
        )
        self.jump: str | int = (
            int(args[1]) if args[1].lstrip("-").isnumeric() else args[1]
        )

    def execute(self, registry: dict[str, int]) -> int:
        value = self.check
        if isinstance(self.check, str):
            value = registry[self.check]

        if value != 0:
            if isinstance(self.jump, str):
                return registry[self.jump]
            else:
                return self.jump
        else:
            return 1


class Day12:
    def __init__(self, data: str):
        self.data = data.splitlines()

    def parse_instructions(self, data: list[str]) -> list[Instruction]:
        instructions: list[Instruction] = []
        instruction_mappings = {
            "cpy": CopyInstruction,
            "inc": IncrementInstruction,
            "dec": DecrementInstruction,
            "jnz": JumpNotZeroInstruction,
        }

        for line in data:
            tokens = line.split()
            instructions.append(instruction_mappings[tokens[0]](*tokens[1:]))

        return instructions

    def solution1(self):
        instructions = self.parse_instructions(self.data)
        cursor = 0
        registry = {"a": 0, "b": 0, "c": 0, "d": 0}
        while cursor < len(instructions):
            cursor += instructions[cursor].execute(registry)
        return registry["a"]

    def solution2(self):
        instructions = self.parse_instructions(self.data)
        cursor = 0
        registry = {"a": 0, "b": 0, "c": 1, "d": 0}
        while cursor < len(instructions):
            cursor += instructions[cursor].execute(registry)
        return registry["a"]
