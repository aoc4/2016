from typing import List, Tuple


class Day2:
    def __init__(self, data: str):
        self.data = data

    def solution1(self):
        return solution1(self.data)

    def solution2(self):
        return solution2(self.data)


def track_coord(
    x: int, y: int, line: str, keypad: List[List[object]]
) -> Tuple[int, int]:
    x_limit = len(keypad[0]) - 1
    y_limit = len(keypad) - 1
    for char in line:
        if char == "U":
            tmp = max(0, y - 1)
            if keypad[tmp][x] is not None:
                y = tmp
        elif char == "D":
            tmp = min(y_limit, y + 1)
            if keypad[tmp][x] is not None:
                y = tmp
        elif char == "L":
            tmp = max(0, x - 1)
            if keypad[y][tmp] is not None:
                x = tmp
        elif char == "R":
            tmp = min(x_limit, x + 1)
            if keypad[y][tmp] is not None:
                x = tmp
    return x, y


def solution1(data: str) -> str:
    x = y = 1
    pin = ""
    keypad = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    for line in data.splitlines():
        x, y = track_coord(x, y, line, keypad)
        pin += str(keypad[y][x])
    return pin


def solution2(data: str) -> str:
    x = 0
    y = 2
    pin = ""
    keypad = [
        [None, None, 1, None, None],
        [None, 2, 3, 4, None],
        [5, 6, 7, 8, 9],
        [None, "A", "B", "C", None],
        [None, None, "D", None, None],
    ]
    for line in data.splitlines():
        x, y = track_coord(x, y, line, keypad)
        pin += str(keypad[y][x])
    return pin
