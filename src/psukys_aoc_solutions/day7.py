# base rules:
# 1. look for XYYX pairs
# 2. IP fails if there are no XYYX pairs
# 3. IP fails if XYYX is inside brackets

from typing import List, Set, Tuple


class Day7:
    def __init__(self, data: str):
        self.data = data.splitlines()

    def solution1(self):
        return solution1(self.data)

    def solution2(self):
        return solution2(self.data)


def _is_abba(letters: str) -> bool:
    return (
        letters[0] == letters[3]
        and letters[1] == letters[2]
        and letters[0] != letters[1]
    )


def _has_abba(substring: str) -> bool:
    if len(substring) < 4:
        return False

    for index in range(len(substring) - 3):  # to have a starter
        if _is_abba(substring[index : index + 4]):
            return True

    return False


def _extract_inside_outside(ip: str) -> Tuple[List[str], List[str]]:
    inside = []
    outside = []
    word = ""
    for letter in ip:
        if letter == "[":
            outside.append(word)
            word = ""
        elif letter == "]":
            inside.append(word)
            word = ""
        else:
            word += letter
    outside.append(word)
    return inside, outside


def _is_abba_ip(ip: str) -> bool:
    # split into portions for inside and outside brackets
    inside, outside = _extract_inside_outside(ip)

    # early exit - abba in brackets
    if any(_has_abba(substring) for substring in inside):
        return False

    if any(_has_abba(substring) for substring in outside):
        return True

    return False  # nothing found - not abba ip


def solution1(ips: List[str]) -> int:
    """return count of valid abba IPs"""
    return sum(_is_abba_ip(ip) for ip in ips)


def _is_aba(letters: str) -> bool:
    return letters[0] == letters[2] and letters[0] != letters[1]


def _has_abba(substring: str) -> bool:
    if len(substring) < 4:
        return False

    for index in range(len(substring) - 3):  # to have a starter
        if _is_abba(substring[index : index + 4]):
            return True

    return False


def _extract_aba(substrings: List[str]) -> Set[str]:
    abas = set()
    for substring in substrings:
        for index in range(len(substring) - 2):
            candidate = substring[index : index + 3]
            if _is_aba(substring[index : index + 3]):
                abas.add(candidate)
    return abas


def _is_aba_bab_pair(aba: str, bab: str) -> bool:
    return aba[0] == bab[1] and aba[1] == bab[0]


def _is_aba_bab_ip(ip: str) -> bool:
    inside, outside = _extract_inside_outside(ip)
    abas = _extract_aba(outside)
    babs = _extract_aba(inside)  # same case, has to be connected later
    for aba in abas:
        for bab in babs:
            if _is_aba_bab_pair(aba, bab):
                return True
    return False


def solution2(ips: List[str]) -> int:
    """return count of valid aba [bab] IPs"""
    return sum(_is_aba_bab_ip(ip) for ip in ips)
